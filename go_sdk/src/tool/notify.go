package tool

import "sync"
// 通告消息处理


type NotifyCB func(map[string]interface{}) (code int, result interface{})

type NotifyMgr struct {
	handleMap map[string]([]NotifyCB)
	lock sync.RWMutex
}

func NewNotifyMgr() NotifyMgr {
	return NotifyMgr{handleMap: make(map[string]([]NotifyCB))}
}

func (mgr *NotifyMgr) AddNotifyCallBack(name string, cb NotifyCB) {
	mgr.lock.Lock()
	defer mgr.lock.Unlock()
	cbArr, ok := mgr.handleMap[name]
	if ok {
		mgr.handleMap[name] = append(cbArr, cb)
	} else {
		mgr.handleMap[name] = []NotifyCB{cb}
	}
}

func (mgr *NotifyMgr) InvokeCallBack(name string, param map[string]interface{}) (
	code int, result interface{}) {
	mgr.lock.RLock()
	defer mgr.lock.RUnlock()

	if cbArr, ok := mgr.handleMap[name]; ok {
		for _,cb := range cbArr {
			code0, result0 := cb(param)
			if nil == result && nil != result0 {
				code, result = code0, result0
			}
		}

		return
	} else {
		return 0, ""
	}
}
